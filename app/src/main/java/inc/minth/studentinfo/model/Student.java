package inc.minth.studentinfo.model;

import java.io.Serializable;

public class Student implements Serializable
{
    private String name;
    private int phone;
    private String className;

    public Student(){}
    public Student(String name, int phone, String className) {
        this.name = name;
        this.phone = phone;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
