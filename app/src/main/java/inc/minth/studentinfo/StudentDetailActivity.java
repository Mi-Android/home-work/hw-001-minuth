package inc.minth.studentinfo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import inc.minth.studentinfo.model.Student;

public class StudentDetailActivity extends AppCompatActivity {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvClass)
    TextView tvClass;

    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_detail);
        ButterKnife.bind(this);
        loadData();
    }
    private void loadData()
    {
        Intent intent=getIntent();
        student=(Student)intent.getSerializableExtra("student");
        tvName.setText(student.getName());
        tvClass.setText(student.getClassName());
        tvPhone.setText(student.getPhone()+"");
    }
    @OnClick(R.id.btnBack)
    public void back()
    {
        Intent intent=new Intent();
        intent.putExtra("student",student);
        setResult(RESULT_OK,intent);
        finish();
    }



}
