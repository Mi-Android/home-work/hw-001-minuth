package inc.minth.studentinfo;

import android.content.Intent;
import android.net.Uri;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import inc.minth.studentinfo.model.Student;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edName)
    EditText edtName;
    @BindView(R.id.edtPhone)
    EditText edtPhone;
    @BindView(R.id.edtClass)
    EditText edtClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btnRegister)
    public void register()
    {
        Student st=new Student(edtName.getText().toString(),Integer.parseInt(edtPhone.getText().toString()),edtClass.getText().toString());
        Intent intent=new Intent(this,StudentDetailActivity.class);
        intent.putExtra("student",st);
        clearText();
        startActivityForResult(intent,1);
    }
    @OnClick(R.id.btnCall)
    public void call()
    {
        Uri phoneNumber= Uri.parse("tel:0964882648");
        Intent intent=new Intent(Intent.ACTION_DIAL,phoneNumber);
        startActivity(intent);
    }
    private void clearText()
    {
        edtClass.setText("");
        edtName.setText("");
        edtPhone.setText("");
    }
    private void setText(Student student)
    {
        edtName.setText(student.getName());
        edtPhone.setText(student.getPhone()+"");
        edtClass.setText(student.getClassName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==1&&resultCode==RESULT_OK)
        {
            Student student=(Student)data.getSerializableExtra("student");
            setText(student);
        }
    }
}
